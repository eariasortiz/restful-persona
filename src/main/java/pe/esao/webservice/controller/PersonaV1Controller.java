package pe.esao.webservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pe.esao.webservice.entity.Persona;
import pe.esao.webservice.services.IPersonaService;
//@RestController
@Controller
@RequestMapping("v1")
public class PersonaV1Controller {

	@Autowired
	private IPersonaService personaService;

	
	@RequestMapping(value="persona/all", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<Persona>> findCategorias() {
		return new ResponseEntity<List<Persona>>(personaService.find(null), HttpStatus.OK);
	}
	/*@Autowired
	private IPersonaService personaService;

	
	@GetMapping(value="persona/all", consumes= {MediaType.ALL_VALUE}, produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE} )
	public ResponseEntity<List<Persona>> findPersonas() {
		return new ResponseEntity<List<Persona>>(personaService.find(null), HttpStatus.OK);
	}*/
}
